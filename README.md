# react-hybrid demo

## Talk points

### What is it?

* React Native and why it is simpler than ReactDOM

### Why should I use it?

* Largely same experience on iOS, Android and web
* You want to build a progressive web app, but also mobile
* It will train you to think in components, not DOM structure
* You can do routing globally

### Why shouldn't I use it?

* If the platforms diverge too much, it may become more problem than it's worth
* Radically different structure per platform
* Existing web application. I imagine migration would be painful.
* There is nobody to cry to, if this breaks. Although main dev is doing this full-time
* If you need server-side-rendering you need to break out of create-react scripts
* If you need to do real native stuff in RN, this will probably be in your way.

### My conclusion

* I think it's a good way to jumpstart a project
* It is good to think outside of the DOM box
* The future may very well be universal components, see react-primitives
* You aren't locked in. If your components are clean (stateless'ish) you can just export them as JS modules and import them into your future project

### Questions?

## Based off

* https://sparkyspace.com/write-once-run-anywhere-with-create-react-native-app-and-react-native-web/
* create-react-app
* create-react-native-app
* expo
