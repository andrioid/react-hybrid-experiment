// This is all based off this guide
// https://sparkyspace.com/write-once-run-anywhere-with-create-react-native-app-and-react-native-web/

import React, { Component } from "react"
import {
	View,
	Text,
	StyleSheet,
	Button,
	TouchableHighlight
} from "react-native"
import { Link, Router, Route, Switch, history } from "./lib/router"
import { AboutContainer } from "./components/about"

export default class App extends Component {
	render() {
		return (
			<View style={styles.app}>
				<Router history={history}>
					<View style={{ flex: 1 }}>
						<View style={styles.appHeader}>
							<View>
								<Text style={{ color: "white", fontSize: 20 }}>
									React Hybrid
								</Text>
							</View>
							<View style={styles.menu}>
								<Button
									style={{ backgroundColor: "orange", color: "pink" }}
									onPress={() => history.push("/")}
									title="Homes"
								/>
								<TouchableHighlight
									style={styles.menuItem}
									onPress={() => history.push("/about")}
								>
									<Text>About</Text>
								</TouchableHighlight>
							</View>
						</View>
						<View style={{ padding: 20, flex: 1 }}>
							<Switch>
								<Route exact path="/about" component={AboutContainer} />
								<Route
									path="/"
									render={() => (
										<View>
											<Text style={styles.appIntro}>Hej Aalborg</Text>
											<Button
												style={{ color: "green" }}
												onPress={() => alert("ouch")}
												title="Click me"
											/>
										</View>
									)}
								/>
							</Switch>
						</View>
					</View>
				</Router>
			</View>
		)
	}
}

const styles = StyleSheet.create({
	app: {
		flex: 1
	},
	appHeader: {
		flex: 1,
		backgroundColor: "#222",
		padding: 20,
		justifyContent: "center",
		alignItems: "center"
	},
	menu: {
		width: "100%",
		flexDirection: "row",
		justifyContent: "center"
	},
	menuItem: {
		marginLeft: 5,
		marginRight: 5,
		backgroundColor: "green",
		padding: 5
	}
})
