// Used by iOS and Android
import { NativeRouter } from "react-router-native"
import createHistory from "history/createMemoryHistory"
export const history = createHistory()
export const Router = NativeRouter
export * from "react-router-native"
